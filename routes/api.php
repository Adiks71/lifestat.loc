<?php

use Illuminate\Http\Request;
use \App\Http\Controllers\Api\AuthController;
use \App\Http\Controllers\Api\HabitsController;
use \App\Http\Controllers\Api\FulfilledsController;
use \App\Http\Controllers\Api\FulfilledsStatisticsController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});
Route::group([

    'middleware' => 'api',
    'prefix' => 'auth'

], function ($router) {

    Route::post('login', [AuthController::class, 'login']);
    Route::post('logout', [AuthController::class, 'logout']);
    Route::post('refresh', [AuthController::class, 'refresh']);
    Route::post('me', [AuthController::class, 'me']);

});

Route::group ([
    'middleware'=>'auth:api',
], function () {

    Route::get('/habits', [HabitsController::class, 'index'])->name('index.habits');
    Route::post('/habits', [HabitsController::class, 'store'])->name('store.habits');
    Route::put('/habits/{id}', [HabitsController::class, 'update'])->name('update.habits');
    Route::get('/habits/{id}', [HabitsController::class, 'show'])->name('show.habits');
    Route::delete('/habits/{id}', [HabitsController::class, 'destroy'])->name('destroy.habits');

    Route::get('/fulfilled_habits/{id}', [FulfilledsController::class, 'show'])->name('show.fulfilled_habits');
    Route::post('/fulfilled_habits', [FulfilledsController::class, 'store'])->name('store.fulfilled_habits');
    Route::delete('/fulfilled_habits/{id}', [FulfilledsController::class, 'destroy'])->name('destroy.fulfilled_habits');
    Route::get('/fulfilled_habits/statistics/calendar', [FulfilledsStatisticsController::class, 'statisticsСalendar'])->name('statistics.calendar.fulfilled');
});

