<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Fulfilleds extends ProjectModel
{
    use HasFactory;
    protected $guarded = [];
    protected $table = 'fulfilleds_habits';
    protected $appends = ['created_at_copy'];

    public function getCreatedAtCopyAttribute($value){

        $created_at_copy = $this->created_at;
        $format_copy = $created_at_copy->format('Y-m-d');
        return $format_copy;
    }

}
