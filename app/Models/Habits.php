<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Habits extends ProjectModel{
    use HasFactory;

        protected $guarded = [];

        protected $table = 'Habits';
}
