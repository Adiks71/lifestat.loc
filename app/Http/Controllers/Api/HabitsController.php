<?php

namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use App\Http\Requests\HabitRequest;
use App\Http\Requests\HabitUpdateRequest;
use App\Models\Habits;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class HabitsController extends Controller {

    public function store(HabitRequest  $request ){
        $user = auth('api')->user();
        $input = $request->input();
        $data = [
            'author_id' => $user -> id,
            'title' => $input['title'],
            'description' => $input['description'],
        ];
        $habit = Habits::query()->create($data);
        return $habit;
    }

    public function show($id, Request $request) {
        $user = auth('api')->user();
        $user_data = Habits::where('author_id', $user['id'])
            ->where('id', $id)
            ->get([
                'title' ,
                'description',
            ]);
        return $user_data;
    }
    public function index(){
        $user = auth('api')->user();
        $user_data =  Habits::query()
            ->where('author_id', $user['id'])
            ->get();
        return $user_data;
    }

    public function update($id, HabitUpdateRequest $request) {
        $user = auth('api')->user();
        $input = $request->only('title', 'description');
        $habit = Habits::where('author_id', $user['id'])
            ->where('id', $id)
        ->first();
            $habit ->update($input);
        return $habit;
    }

    public function destroy($id) {
        $user = auth('api')->user();
                $habit = Habits::findOrFail($id)
                    ->where('author_id', $user['id'])
                    ->where('id', $id)
                    ->first();
                $habit->delete();
                return $habit;
    }
}