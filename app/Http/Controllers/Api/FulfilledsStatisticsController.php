<?php

namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use App\Models\Fulfilleds;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Illuminate\Http\Request;

class FulfilledsStatisticsController extends Controller{

    public function statisticsСalendar(){
        $user = auth('api')->user();
        $graph_calendar = Fulfilleds::select(['created_at'])
            ->where('author_id', $user['id'])
            ->get()
            ->toArray();
        $current_date = Carbon::today();
        $Date = Carbon::today();
        $endDate = $Date->modify('-100days');
        $period = CarbonPeriod::create($endDate, $current_date);
        $array_dates = [];
            foreach ($period as $date) {
                $array_dates[] = $date->format('Y-m-d');
            }
        $number_day = [];
            foreach ($period as $date) {
                $number_day[] =  $date->format('d');
            }
        $for_graph = [];
        $array_dates_length =  count($array_dates);
        for($i=0;$i < $array_dates_length;$i++){
            $match_date = false;
            $graph_calendar_length = count($graph_calendar);
            for($y=0;$y < $graph_calendar_length;$y++) {
                if($array_dates[$i] === $graph_calendar[$y]['created_at_copy']){
                    $match_date = true;
                }
            }
            $for_graph[] = [
                'date' => $array_dates[$i],
                'number' => $number_day[$i],
                'is_fulfilled' =>$match_date,
            ];
        }
   }
}


