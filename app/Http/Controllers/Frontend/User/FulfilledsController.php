<?php

namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use App\Http\Requests\FulfilledsRequest;
use App\Models\Fulfilleds;
use Illuminate\Http\Request;
class FulfilledsController extends Controller
{
    public function store(FulfilledsRequest  $request ){
        $user = auth('api')->user();
        $input = $request->input();
        $data = [
            'author_id' => $user -> id,
            'habit_id' => $input['habit_id'],
            'duration' => $input['duration'],
        ];
        $habit = Fulfilleds::query()->create($data);
        return $habit;
    }

    public function show($id, Request $request) {
        $user = auth('api')->user();
        $user_data = Fulfilleds::where('author_id', $user['id'])
            ->where('id', $id)
            ->get([
                'habit_id' ,
                'duration',
            ]);
        return $user_data;
    }

    public function destroy($id) {
        $user = auth('api')->user();
        $habit = Fulfilleds::findOrFail($id)
            ->where('author_id', $user['id'])
            ->where('id', $id)
            ->first();
        $habit->delete();
        return $habit;
    }


}
